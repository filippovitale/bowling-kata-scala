import org.specs2.Specification
import org.specs2.specification._

class BowlingGameTest extends Specification with Tables {
  def is = s2"""
    Few pins        $step1
    Few spares      $step2
    Few strikes     $step3
    Frames as text  $step4
    A simple game   $step5
  """

  def step1 = {
      // format: OFF
      "pins"                  | "score" |>
      List(1)                 !      1  |
      List(1, 3)              !      4  |
      List(1, 3, 5, 2, 1)     !     12  |
      List(1, 0, 5, 0)        !      6  |
      List(0, 0)              !      1  |
      // format: ON
      { BowlingGame.score(_) ==== _ }
  }

  def step2 = {
      // format: OFF
      "pins"                  | "score" |>
      List(1, 9)              !     10  |
      List(1, 9, 0, 0)        !     10  |
      List(1, 9, 0, 5)        !     15  |
      List(1, 9, 3, 5, 0)     !     21  |
      List(1, 9, 3, 7, 2, 3)  !     30  |
      // format: ON
      { BowlingGame.score(_) ==== _ }
  }

  def step3 = {
      // format: OFF
      "pins"                  | "score" |>
      List(10)                !     10  |
      List(10, 0, 0)          !     10  |
      List(10, 0, 0, 5, 1)    !     16  |
      List(10, 5, 1)          !     22  |
      // format: ON
      { BowlingGame.score(_) ==== _ }
  }

  def step4 = {
      // format: OFF
      "pins"    | "score" |>
      "--"      !      0  |
      "1"       !      1  |
      "13"      !      4  |
      "13521"   !     12  |
      "1-5-"    !      6  |
      "1/"      !     10  |
      "1/--"    !     10  |
      "1/-5"    !     15  |
      "1/35-"   !     21  |
      "1/3/23"  !     30  |
      "X"       !     10  |
      "X--"     !     10  |
      "X--51"   !     16  |
      "X51"     !     22  |
      "XX42"    !     46  |
      // format: ON
      { BowlingGame.score(_) ==== _ }
  }

  def step5 = {
      // format: OFF
      "pins"                 | "score" |>
      "1/35XXX45"            !    103  |
      "11111111112222222222" !     30  |
      "--------------------" !      0  |
      "1-1----------------1" !      3  |
      "9-9-9-9-9-9-9-9-9-9-" !     90  |
      "5/11------------3/11" !     26  |
      //"919-9-9-9-9-929-9-9-"!    93  |
      "9-8/--9-9-9-9-9-9-9-" !     82  |
      "--8/1---------------" !     12  |
      "--8/-1--------------" !     11  |
      "9-X8-9-9-9-9-9-9-9-"  !     98  |
      "--X81--------------"  !     28  |
      "--X8-1-------------"  !     27  |
      // format: ON
      { BowlingGame.score(_) ==== _ }
  }

}
