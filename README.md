## Bowling Scoring

- Each game, or "line" of bowling, includes ten turns, or "frames" for the bowler.
- In each frame, the bowler gets up to two tries to knock down all the pins.
- If in two tries, he fails to knock them all down, 
     - his score for that frame is the total number of pins knocked down in his two tries.
- If in two tries he knocks them all down,
     - this is called a "spare"
     - his score for the frame is ten plus the number of pins knocked down on his next throw (in his next turn).
- If on his first try in the frame he knocks down all the pins,
     - this is called a "strike".
     - His turn is over, and his score for the frame is ten plus the simple total of the pins knocked down in his next two rolls.
- If he gets a spare or strike in the last (tenth) frame,
     - the bowler gets to throw one or two more bonus balls, respectively.
     - These bonus throws are taken as part of the same turn.
     - If the bonus throws knock down all the pins, the process does not repeat:
        - the bonus throws are only used to calculate the score of the final frame.
- The game score is the total of all frame scores.

Source: http://codingdojo.org/cgi-bin/index.pl?KataBowling

More info on the rules at: www.topendsports.com/sport/tenpin/scoring.htm

## Steps

### Step 1

~~~
$ sbt ~test
[info] BowlingGameTest
[error]     x Few pins
[error]    | pins                | score |
[error]  x | List(1)             | 1     | an implementation is missing
[error]  x | List(1, 3)          | 4     | an implementation is missing
[error]  x | List(1, 3, 5, 2, 1) | 12    | an implementation is missing
[error]  x | List(1, 0, 5, 0)    | 6     | an implementation is missing
[error]  x | List(0, 0)          | 1     | an implementation is missing (BowlingGame.scala:3)
~~~

### Step 2

~~~
$ sbt ~test
[info] BowlingGameTest
[error]     x Few spares
[error]    | pins                   | score |
[error]  x | List(1, 9)             | 10    | an implementation is missing
[error]  x | List(1, 9, 0, 0)       | 10    | an implementation is missing
[error]  x | List(1, 9, 0, 5)       | 15    | an implementation is missing
[error]  x | List(1, 9, 3, 5, 0)    | 21    | an implementation is missing
[error]  x | List(1, 9, 3, 7, 2, 3) | 30    | an implementation is missing (BowlingGame.scala:3)
~~~

### Step 3

~~~
$ sbt ~test
[info] BowlingGameTest
[error]     x Few strikes
[error]    | pins                 | score |
[error]  x | List(10)             | 10    | an implementation is missing
[error]  x | List(10, 0, 0)       | 10    | an implementation is missing
[error]  x | List(10, 0, 0, 5, 1) | 16    | an implementation is missing
[error]  x | List(10, 5, 1)       | 22    | an implementation is missing (BowlingGame.scala:3)
~~~

### Step 4

~~~
$ sbt ~test
[info] BowlingGameTest
[error]     x Frames as text
[error]    | pins   | score |
[error]  x | --     | 0     | an implementation is missing
[error]  x | 1      | 1     | an implementation is missing
[error]  x | 13     | 4     | an implementation is missing
[error]  x | 13521  | 12    | an implementation is missing
[error]  x | 1-5-   | 6     | an implementation is missing
[error]  x | 1/     | 10    | an implementation is missing
[error]  x | 1/--   | 10    | an implementation is missing
[error]  x | 1/-5   | 15    | an implementation is missing
[error]  x | 1/35-  | 21    | an implementation is missing
[error]  x | 1/3/23 | 30    | an implementation is missing
[error]  x | X      | 10    | an implementation is missing
[error]  x | X--    | 10    | an implementation is missing
[error]  x | X--51  | 16    | an implementation is missing
[error]  x | X51    | 22    | an implementation is missing
[error]  x | XX42   | 46    | an implementation is missing (BowlingGame.scala:5)
~~~

### Step 5

~~~
$ sbt ~test
[info] BowlingGameTest
[error]     x A simple game
[error]    | pins                 | score |
[error]  x | 1/35XXX45            | 103   | an implementation is missing
[error]  x | 11111111112222222222 | 30    | an implementation is missing
[error]  x | -------------------- | 0     | an implementation is missing
[error]  x | 1-1----------------1 | 3     | an implementation is missing
[error]  x | 9-9-9-9-9-9-9-9-9-9- | 90    | an implementation is missing
[error]  x | 5/11------------3/11 | 26    | an implementation is missing
[error]  x | 9-8/--9-9-9-9-9-9-9- | 82    | an implementation is missing
[error]  x | --8/1--------------- | 12    | an implementation is missing
[error]  x | --8/-1-------------- | 11    | an implementation is missing
[error]  x | 9-X8-9-9-9-9-9-9-9-  | 98    | an implementation is missing
[error]  x | --X81--------------  | 28    | an implementation is missing
[error]  x | --X8-1-------------  | 27    | an implementation is missing (BowlingGame.scala:5)
~~~

## Inspiration and Reference

- https://bitbucket.org/michelemauro/bowling-kata-scala
- http://codingdojo.org/cgi-bin/index.pl?KataBowling
