name := "bowling"

version := "0.0.1"

scalaVersion := "2.11.8"

scalacOptions := Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xfuture",
  "-Xlint",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-unused",
  "-Ywarn-unused-import",
  "-Ywarn-value-discard"
)

wartremoverWarnings ++= Warts.unsafe

libraryDependencies ++= Seq(
  "org.specs2"     %%  "specs2-core"        % "3.7.3"  % "test",
  "org.specs2"     %%  "specs2-scalacheck"  % "3.7.3"  % "test",
  "org.scalacheck" %%  "scalacheck"         % "1.13.1" % "test" 
)
